package com.everis.reactive.game.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.everis.reactive.game.entitty.dto.GamePlayer;
import com.everis.reactive.game.entitty.dto.PlayerForm;
import com.everis.reactive.game.service.PositionServiceContract;

import reactor.core.publisher.Flux;

@RestController
@RequestMapping("/game")
public class GamePositionAPIController {

	@Autowired
	PositionServiceContract registerService;

	@GetMapping(value = "/position", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
	@CrossOrigin(origins = "*")
	public Flux<Object> getAllPlayerPositions() {
		return Flux.merge(registerService.loadAllPosition(),registerService.getReactiveGamePlayerPositionLog());
	}

	
	@GetMapping(value = "/position/player/{identification}/{user}", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
	@CrossOrigin(origins = "*")
	public Flux<GamePlayer> getPositionByPlayer(@PathVariable String identification, @PathVariable String user) {
		GamePlayer gamer = new GamePlayer();
		PlayerForm player = new PlayerForm();
		player.setIdentification(identification);
		player.setUser(user);
		gamer.setPlayer(player);
		return registerService.loadPositionByGamePlayer(gamer);
	}
	
	@GetMapping(value = "/position/status", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
	@CrossOrigin(origins = "*")
	public Flux<Object> getStatusForPlayerPositions() {
		return Flux.merge(registerService.loadStatusForPlayerPosition(),registerService.getReactiveStatusLog());
	}

}
