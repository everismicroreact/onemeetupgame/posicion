package com.everis.reactive.game.service.exceptions;

public class ThereIsNoPositionAvailableException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ThereIsNoPositionAvailableException() {
		super();
	}

	public ThereIsNoPositionAvailableException(String message) {
		super(message);
	}

	public ThereIsNoPositionAvailableException(Throwable cause) {
		super(cause);
	}

	public ThereIsNoPositionAvailableException(String message, Throwable cause) {
		super(message, cause);
	}

	public ThereIsNoPositionAvailableException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
