package com.everis.reactive.game.service;

import org.springframework.http.codec.ServerSentEvent;

import com.everis.reactive.game.entitty.dto.GamePlayer;

import reactor.core.publisher.Flux;

public interface PositionServiceContract {

	public Flux<GamePlayer> loadAllPosition();

	public Flux<GamePlayer> loadPositionByGamePlayer(GamePlayer gamePlayer);

	public Flux<GamePlayer> loadTopPosition(int nPositions);

	public Flux<Boolean> loadStatusForPlayerPosition();

	public Flux<ServerSentEvent<Boolean>> getReactiveStatusLog();
	
	public Flux<ServerSentEvent<GamePlayer>> getReactiveGamePlayerPositionLog();

}
