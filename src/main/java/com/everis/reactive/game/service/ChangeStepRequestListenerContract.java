package com.everis.reactive.game.service;

import com.everis.reactive.game.entitty.dto.GamePlayer;

public interface ChangeStepRequestListenerContract {

	public void requestForStep(GamePlayer gamePlayer);

}
