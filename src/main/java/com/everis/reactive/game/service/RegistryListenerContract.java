package com.everis.reactive.game.service;

import com.everis.reactive.game.entitty.dto.GamePlayer;

public interface RegistryListenerContract {

	public void registerGamePlayerListener(GamePlayer gamePlayer);

}
