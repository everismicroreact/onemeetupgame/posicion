package com.everis.reactive.game.service;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.sample;

import java.util.Comparator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.http.codec.ServerSentEvent;
import org.springframework.stereotype.Service;

import com.everis.reactive.game.entitty.dto.CorrectAnswerMessage;
import com.everis.reactive.game.entitty.dto.GamePlayer;
import com.everis.reactive.game.entitty.dto.PositionForm;
import com.everis.reactive.game.entitty.orm.PositionStatus;
import com.everis.reactive.game.repository.ReactivePositionRepository;
import com.everis.reactive.game.repository.ReactivePositionStatusRepository;

import reactor.core.publisher.EmitterProcessor;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class PositionService implements PositionServiceContract, RegistryListenerContract {

	@Value("${com.everis.reactive.game.core.steps}")
	private int steps;

	@Autowired
	ReactivePositionRepository positionRepository;

	@Autowired
	ReactivePositionStatusRepository positionStatusRepository;

	private final EmitterProcessor<ServerSentEvent<Boolean>> emitterPositionStatus = EmitterProcessor.create(false);

	private final EmitterProcessor<ServerSentEvent<GamePlayer>> emitterPosition = EmitterProcessor.create(false);

	@Autowired
	private MongoTemplate mongoTemplate;

	@StreamListener(target = Sink.INPUT, condition = "headers['operation']=='registry'")
	public void registerGamePlayerListener(GamePlayer gamePlayer) {
		// se le asigna una pista, una linea y se ubica en posicion 0
		PositionForm position = new PositionForm();
		position.setLine(1);
		position.setTrack(1);
		// ubicar en posicion inicial
		position.setStep(0);
		gamePlayer.setPosition(position);
		gamePlayer.setPoints(0);

		positionRepository.save(gamePlayer).subscribe();
		emitterPosition.onNext(ServerSentEvent.builder(gamePlayer).build());
		System.out.println("Se ha registrado un nuevo usuari");

	}

	@StreamListener(target = Sink.INPUT, condition = "headers['operation']=='challenge'")
	public void WinChallengeListener(CorrectAnswerMessage message) {
		GamePlayer gamePlayer = new GamePlayer();
		gamePlayer.setPlayer(message.getPlayer());
		positionRepository.findById(message.getPlayer()).flatMap(x -> {
			return adjustPosition(x, message);
		}).subscribe(System.out::println);

		
	}

	@Override
	public Flux<GamePlayer> loadAllPosition() {
//		return Flux.interval(Duration.ofSeconds(1)).onBackpressureDrop().map(this::generateStreamForFindAllPosition)
//				.flatMap(x -> x);
		return positionRepository.findAll();
	}


	private Mono<GamePlayer> adjustPosition(GamePlayer gamePlayer, CorrectAnswerMessage message) {
		PositionForm newPosition = gamePlayer.getPosition();
		return positionStatusRepository.findAll().filter(x -> x.isEnabled()).next().map(x -> {

			newPosition.setStep(newPosition.getStep() + 1);
			// calcular puntaje: el avance que lleve (steps) dividido por el tiempo que
			// gasto en la respuesta / miliseconds multiplicado por mil * 1000 y sumado al
			// puntaje anterior + prev.getPoints
			int currentSteps = newPosition.getStep();
			System.out.println("curentST:" + currentSteps);
			int currentMiliseconds = message.getMiliseconds();
			System.out.println("curentMS:" + currentMiliseconds);
			float pointsThisStep = ((float) currentSteps / ((float) currentMiliseconds <= 0 ? 10 : currentMiliseconds))
					* 10000.0f;
			System.out.println("previo:" + pointsThisStep);
			gamePlayer.setPoints(gamePlayer.getPoints() + (int) Math.round(pointsThisStep));
			System.out.println("cambiando de posicion");

			System.out.println("Ajustando posicion" + gamePlayer.getPosition().getStep());
			
			
			System.out.println("La posicion sera ajustada a " + gamePlayer.getPosition().getStep());

			

			if (gamePlayer.getPosition().getStep() >= steps) {
				Aggregation agg = newAggregation(sample(1L));
				AggregationResults<PositionStatus> results = mongoTemplate.aggregate(agg, PositionStatus.class,
						PositionStatus.class);
				PositionStatus positionStatus = results.getUniqueMappedResult();
				positionStatus.setEnabled(false);
				positionStatusRepository.save(positionStatus).subscribe();

				emitterPositionStatus.onNext(ServerSentEvent.builder(positionStatus.isEnabled()).build());
				System.out.println("terminando juego");

			}else
			{
				positionRepository.save(gamePlayer).subscribe();

				emitterPosition.onNext(ServerSentEvent.builder(gamePlayer).build());
				System.out.println("La posicion ha sido ajustada" + gamePlayer.getPosition().getStep());
			}

			return x;
		}).cast(GamePlayer.class);

	}

	@Override
	public Flux<GamePlayer> loadPositionByGamePlayer(GamePlayer gamePlayer) {
//		return Flux.interval(Duration.ofSeconds(1)).onBackpressureDrop()
//				.map(x -> generateStreamForFindByIdPosition(gamePlayer.getPlayer(), x)).flatMap(x -> x);
		return positionRepository.findById(gamePlayer.getPlayer()).flux();

	}

	@Override
	public Flux<GamePlayer> loadTopPosition(int nPositions) {
//		return Flux.interval(Duration.ofSeconds(1)).onBackpressureDrop()
//				.map(this::generateStreamForFindAllPositionSorted).flatMap(x -> x);
		return positionRepository.findAll().sort(Comparator.comparing(GamePlayer::getPoints)).map(x -> x);
//		return positionRepository.findAll().sort(Comparator.comparing(GamePlayer::getPoints));
	}

	@Override
	public Flux<Boolean> loadStatusForPlayerPosition() {
//		return Flux.interval(Duration.ofSeconds(1)).onBackpressureDrop().map(this::cargarStatus).flatMap(x -> x)
//				.map(x -> x.isEnabled());
		return positionStatusRepository.findAll().map(x -> x).map(x -> x.isEnabled());
	}


	@Override
	public Flux<ServerSentEvent<Boolean>> getReactiveStatusLog() {
		return emitterPositionStatus.log();
	}

	@Override
	public Flux<ServerSentEvent<GamePlayer>> getReactiveGamePlayerPositionLog() {
		return emitterPosition.log();
	}

}
