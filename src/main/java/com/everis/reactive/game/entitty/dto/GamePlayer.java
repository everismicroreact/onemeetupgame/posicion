package com.everis.reactive.game.entitty.dto;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="Positions")
public class GamePlayer{
	@Id
//	private String id;
	
	private PlayerForm player;
	private VehicleForm vehicle;
	private PositionForm position;
	private int points;

	public PlayerForm getPlayer() {
		return player;
	}

	public void setPlayer(PlayerForm player) {
		this.player = player;
	}

	public VehicleForm getVehicle() {
		return vehicle;
	}

	public void setVehicle(VehicleForm vehicle) {
		this.vehicle = vehicle;
	}

	public PositionForm getPosition() {
		return position;
	}

	public void setPosition(PositionForm position) {
		this.position = position;
	}

	public int getPoints() {
		return points;
	}

	public void setPoints(int points) {
		this.points = points;
	}

//	public String getId() {
//		return id;
//	}
//
//	public void setId(String id) {
//		this.id = id;
//	}

}
