package com.everis.reactive.game.entitty.dto;

public class PositionForm {
	/*
	 * identifica la pista a la cual el vehiculo pertenece. En una pista pueden
	 * haber una cantidad configurable de carros
	 */
	private int track;
	/**
	 * indica la linea sobre la cual esta transitando el vehiculo.
	 */
	private int line;
	/*
	 * Distancia recorrida, medida en pasos.
	 */
	private int step;

	public int getTrack() {
		return track;
	}

	public void setTrack(int track) {
		this.track = track;
	}

	public int getLine() {
		return line;
	}

	public void setLine(int line) {
		this.line = line;
	}

	public int getStep() {
		return step;
	}

	public void setStep(int step) {
		this.step = step;
	}

}
