package com.everis.reactive.game.repository;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

import com.everis.reactive.game.entitty.dto.GamePlayer;
import com.everis.reactive.game.entitty.dto.PlayerForm;

public interface ReactivePositionRepository extends ReactiveMongoRepository<GamePlayer, PlayerForm> {

}
