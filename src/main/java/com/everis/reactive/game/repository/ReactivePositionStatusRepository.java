package com.everis.reactive.game.repository;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

import com.everis.reactive.game.entitty.orm.PositionStatus;

public interface ReactivePositionStatusRepository extends ReactiveMongoRepository<PositionStatus, String> {

}
