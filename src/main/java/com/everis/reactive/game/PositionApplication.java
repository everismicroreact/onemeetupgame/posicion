package com.everis.reactive.game;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableBinding(Sink.class)
@EnableTransactionManagement
@EnableReactiveMongoRepositories
public class PositionApplication {

	public static void main(String[] args) {
		SpringApplication.run(PositionApplication.class, args);
	}

}
