El servicio de posición reacciona al registro de usuarios y a la respuesta correcta de los desafios. 
Requiere una base de datos en Mongo y escuchar los topicos en kafka escritos por los microservicios de Registro y Preguntas.

**Base de datos Mongo**

*Esquema Positions*

Almacena la ubicación de los jugadores en todo momento durante el juego, asi como su puntaje e información para identificarlo. Esta colección no requiere inicializarse, pero debe existir en mongo.

*Esquema PositionStatus*

Permite notificar al juego si se está habilitado el movimiento del jugador. Se usa principalmente para arrancar el juego y detenerlo cuando un jugador lo gana. Se debe inicializar en true con el siguiente esquema:

```
{
 "id": ObjectId("5d6c5c9f1c9d4400009a68cf")
 "enable": "true";
}
```

**Nota**. Tenga en cuenta que el atributo enable debe ser booleano y no un string.


**Docker**

La imagen de docker para este microservicio se genera a partir del Siguiente Dockerfile, que hace referencia al paquete jar generado para la última versión en código estable. Durante la ejecución, recibe 14 parametros que asigna a variables de entorno que deben enviarse al momento de levantar el contenedor. En el archivo Dockerfile se incluye valores por defecto que seguramente no funcionarán en su ambiente.

```
FROM openjdk:8u131-jre-alpine
COPY ./position-0.0.1-SNAPSHOT.jar app.jar


ENV MEETUP_SERVER_PORT	8081
ENV MEETUP_KAFKA_SERVER	192.168.0.50
ENV MEETUP_KAFKA_PORT	9092
ENV MEETUP_KAFKA_TOPIC_CHALLENGE challenge
ENV MEETUP_KAFKA_TOPIC_REGISTRY	registry
ENV MEETUP_MONGO_USER	game
ENV MEETUP_MONGO_PASSWORD "set_pass"
ENV MEETUP_MONGO_SERVER	"set_server"
ENV MEETUP_MONGO_PORT	27017
ENV MEETUP_MONGO_DB game
ENV MEETUP_MONGO_RW true
ENV MEETUP_MONGO_W majority
ENV MEETUP_MONGO_SSTO 600000
ENV MEETUP_MONGO_FORMAT_CONNECTION mongodb
ENV MEETUP_GAME_STEPS	20

EXPOSE $MEETUP_SERVER_PORT
CMD ["/usr/bin/java", "-jar", "app.jar"]
```

**Build de este Dockerfile**:

`docker build -f Dockerfile -t meetup/posicion .`

**Run de este Dockerfile**:

`docker run -d --name posicion -e MEETUP_KAFKA_SERVER=[IP KAFKA] -e MEETUP_KAFKA_PORT=[PUERTO KAFKA] -e MEETUP_KAFKA_TOPIC_CHALLENGE=challenge -e MEETUP_KAFKA_TOPIC_REGISTRY=registry -e MEETUP_MONGO_USER=[USUARIO BD MONGO] -e MEETUP_MONGO_PASSWORD=[PASSWORD USUARIO MONGO] -e MEETUP_MONGO_SERVER=[IP SERVER MONGO] -e MEETUP_MONGO_DB=[NOMBRE BD MONGO] -e MEETUP_GAME_STEPS=[NUMERO DE PASOS PARA LLEGAR A LA META] -e MEETUP_MONGO_FORMAT_CONNECTION=[FORMATO DE CONEXIÓN CON MONGO] -e MEETUP_SERVER_PORT=[PUERTO HTTP] -p 8091:8091 meetup/posicion`

**Argumentos**

* MEETUP_KAFKA_SERVER	Ip del servidor de kafka
* MEETUP_KAFKA_PORT	Puerto para el servidor kafka
* MEETUP_KAFKA_TOPIC_CHALLENGE nombre del topico para las preguntas respondidas correctamente. por defecto challenge
* MEETUP_KAFKA_TOPIC_REGISTRY nombre del topico que escucha cuando se registra un jugador. por defecto registry
* MEETUP_MONGO_USER	Nombre de usuario para la base de datos mongo
* MEETUP_MONGO_PASSWORD password para autorizar el usuario de la base de datos mongo
* MEETUP_MONGO_SERVER	DNS o ip del servidor
* MEETUP_MONGO_DB nombre de la base de datos en mongo
* MEETUP_MONGO_RW Valor para retryWrites en mongo, por defecto true
* MEETUP_MONGO_W valor para w en mongo, por defecto majority
* MEETUP_MONGO_SSTO valor para serverSelectionTimeoutMS en mongo, por defecto 2000
* MEETUP_MONGO_CTO valor para connectTimeoutMS en mongo, por defecto 2000
* MEETUP_MONGO_FORMAT_CONNECTION formato de conexión. Valores posibles: mongodb, mongodb+srv
* MEETUP_GAME_STEPS total de jugadas o pasos que se le permitirá avanzar a los jugadores antes de llegar a la meta. por defecto 20.
* MEETUP_SERVER_PORT Puerto http para exponer el servicio